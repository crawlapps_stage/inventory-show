<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify'])->name('home');

Route::group(['middleware' => ['auth.shopify']], function () {
    Route::get('/products', 'Product\ProductController@index');
    Route::get('/variants', 'Product\ProductController@getVariants');
});

Route::get('flush', function(){
    request()->session()->flush();
});
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
