<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Traits\GraphQLTrait;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use GraphQLTrait;
    public function index(Request $request){
        try{
            $shop = \Auth::user();
            if ($request->s != '') {
                $parameter['title'] = $request->s;
            } else {
                if( $request->order == 0){
                    $parameter['limit'] = $request->per_page;
                    $parameter['page_info'] = ($request->page) ? $request->page : '';
                }
            }
            $api_version = '2019-04';
            if ($request->order == 0){
                if( $request->s != '' ){
                    $api_version = '2019-04';
                }else{
                    $api_version = '2019-07';
                }
            } elseif ($request->order == 1){
                $api_version = '2019-04';
                $order = 'title desc';
            } elseif ($request->order == 2){
                $api_version = '2019-07';
                $order = 'inventory_total desc';
            } elseif ($request->order == 3){
                $api_version = '2019-07';
                $order = 'inventory_total asc';
            }
            $endPoint = '/admin/api/'.$api_version.'/products.json';

            if( $request->order != 0 ){
                $parameter['order'] = $order;
            }
            $parameter['fields'] = "id,title,image,handle,product_type,vendor,variants";

            $products = $shop->api()->rest('GET', $endPoint, $parameter);
            if ($products['body']->container['products']) {

                foreach ($products['body']->container['products'] as $pk => $pv) {
                    $total_variant = count($pv['variants']);
                    $stock = 0;
                    foreach ($pv['variants'] as $tvkey=>$tvval){
                        $stock += $tvval['inventory_quantity'];
                    }
                    $product[$pk]['id'] = $pv['id'];
                    $product[$pk]['title'] = $pv['title'];
                    $product[$pk]['image'] = ($pv['image']) ? $pv['image']['src'] : '/static_upload/no-image-box.png';
                    $product[$pk]['handle'] = $pv['handle'];
                    $product[$pk]['type'] = $pv['product_type'];
                    $product[$pk]['inventory'] = ($stock > 0 ) ? $stock . ' in stock for ' . $total_variant . ' variants' : 'Inventory not tracked';
                    $product[$pk]['vendor'] = $pv['vendor'];
                }
            } else {
                $product = [];
            }

            $data['next'] = ($products['link']) ? $products['link']->next : '';
            $data['prev'] = ($products['link']) ? $products['link']->previous : '';
            $data['product'] = $product;
            return response()->json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }


    public function getVariants(Request $request)
    {
        try {
            $shop = \Auth::user();
            $product_id = $request['product_id'];
            $parameter['fields'] = 'id,title,price,inventory_quantity';
            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'. $product_id .'/variants.json';
            $variants = $shop->api()->rest('GET', $endPoint, $parameter);

            if ($variants['body']->container['variants']) {
                foreach ($variants['body']->container['variants'] as $vkey => $vval) {
                    $variant[$vkey]['id'] = $vval['id'];
                    $variant[$vkey]['title'] = $vval['title'];
                    $variant[$vkey]['inventory'] = $vval['inventory_quantity'];
                    $variant[$vkey]['price'] = $vval['price'];
                }
            } else {
                $variant = [];
            }
            $data['variant'] = $variant;

            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e], 422);
        }
    }
}
